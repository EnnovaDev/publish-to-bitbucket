# Publish to BitBucket

In this repo you will find two scripts: both serve the same purpose, but the first one will work on any repository, while the second is specific to Ennova, and will load the artifacts in the `EnnovaDev/maven_repository` repo.

Please refer to [the guide][guide] in order to be able to use these scripts.

[guide]:https://ennova-dev.atlassian.net/wiki/display/DEV/Caricare+una+libreria+sul+repository+Maven+di+Ennova
